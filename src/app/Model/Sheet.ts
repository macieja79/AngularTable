
import { SheetRow } from "./SheetRow";

export class Sheet {

    name: string;
    rows : SheetRow[] = [];

}