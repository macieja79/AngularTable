import { GuidFactory } from "./GuidFactory";
import { SheetCell } from "./SheetCell";

export class SheetRow {
   
    cells : SheetCell[] = [];
      
    public Sum(other: SheetRow) : void{
        var numberOfCells = this.cells.length;
        for(var c = 0; c < numberOfCells; c++){
            this.cells[c].value += other.cells[c].value;
        }
    }

    public CopyFrom(other: SheetRow) : void{
        var numberOfCells = this.cells.length;
        for(var c = 0; c < numberOfCells; c++){
            this.cells[c].value = other.cells[c].value;
        }
    }

    public static Create (numberOfItems : number) : SheetRow {
        var row = new SheetRow();
        for (var i = 0; i < numberOfItems; i++)
        {
            var cell : SheetCell = {value: 0};
            row.cells.push(cell);
        }
        return row;
    }

    public static  CreateInc (numberOfItems : number, start : number ) : SheetRow {
        var row = new SheetRow();
        var n = start;
        for (var i = 0; i < numberOfItems; i++)
        {
            var cell : SheetCell = {value: n};
            row.cells.push(cell);
            n = n + 1;
        }
        return row;
    }

}