import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { SheetRow } from "../../Model/SheetRow";
import { Consts } from "../../Model/Consts";
import { GuidFactory } from "../../Model/GuidFactory";


@Component({
  selector: 'my-summary',
  styleUrls: ['../../Sheet/sheet.component.css'],
  templateUrl: './sheetSummary.component.html'
})

export class SheetSummaryComponent implements OnChanges {

  headerRow: SheetRow = SheetRow.CreateInc(Consts.NumberOfColumns, 1);
  
  @Input() summaryRow : SheetRow = SheetRow.Create(Consts.NumberOfColumns);

  public total:number = 0;

  ngOnChanges(changes: SimpleChanges): void {
    this.recalculate();  
  }

  private recalculate() : void {
      var sum = 0;
      for (var i = 0; i < this.summaryRow.cells.length; i++)
      {
          sum += this.summaryRow.cells[i].value;
      }
      this.total = sum;
  }

}
