import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { SheetRow } from "../../Model/SheetRow";
import { GuidFactory } from "../../Model/GuidFactory";
import { SheetService } from "../../Services/sheet.service";
import { Consts } from "../../Model/Consts";

@Component({
  selector: 'my-sheet',
  styleUrls: ['./sheet.component.css'],
  templateUrl: './sheet.component.html'
})

export class SheetComponent implements OnInit {

  private _currentRow : number = -1;
  private _currentCol: number = -1;

  constructor (private sheetService : SheetService) { }

  @Input() guidId : string = GuidFactory.newGuid();  
  @Input() name: string;

  headerRow: SheetRow = SheetRow.CreateInc(Consts.NumberOfColumns, 1);
  dataRows: SheetRow[] =[SheetRow.Create(Consts.NumberOfColumns)];
  summaryRow: SheetRow = SheetRow.Create(Consts.NumberOfColumns) ;
  
  @Input() isData : boolean;

  @Output() eventLastRowDeleted = new EventEmitter();
  @Output() eventCellValueChanged = new EventEmitter();
  @Output() eventSheetDeleted = new EventEmitter<string>();

  recalculate() : void{

    var numberOfColumns = this.summaryRow.cells.length;
    var numberOfRows = this.dataRows.length;

    for (var c = 0; c < numberOfColumns; c++)
    {
        var columnSum : number = 0;
        for (var r = 0; r < numberOfRows; r++) 
        {
            columnSum += this.dataRows[r].cells[c].value;
        }

        this.summaryRow.cells[c].value = columnSum;
    }

    this.sheetService.updateSummary(this.guidId, this.summaryRow);
    this.eventCellValueChanged.emit("xxx");

  }

  ngOnInit(): void {
      this.name = this.sheetService.getName();
      this.recalculate();
  }

  onAddButtonClick(val:any) : void {
      this.addRow();
  }

  onDelButtonClick(val:any) : void {
      this.deleteRow();
  }

  onCopyButtonClick() : void {
     this.copyRow();  
  }

  onUpButtonClick(){
    var old_index : number = this._currentRow;
    var new_index = old_index - 1;
    this.dataRows.splice(new_index, 0, this.dataRows.splice(old_index, 1)[0]);
  }

  onDownButtonClick(){
    var old_index : number = this._currentRow;
    var new_index = old_index + 1;
    this.dataRows.splice(new_index, 0, this.dataRows.splice(old_index, 1)[0]);
  }

  onSheetDeleteButtonClick() : void{
      this.eventSheetDeleted.emit(this.guidId);
  }

  

  onCellChange() : void {
      this.recalculate();
  }



  onFocus(val:any) : void{
     
      var row : number = parseInt(val.getAttribute("data-row"));
      var col : number = parseInt(val.getAttribute("data-col"));

      this._currentRow = row;
      this._currentCol = col;
   
      this.recalculate();
  }




    addRow() : void {
      var newRow : SheetRow = SheetRow.Create(Consts.NumberOfColumns);
      this.dataRows.push(newRow);
      this.recalculate();
    }

     deleteRow() : void {

      var rowIndexToDelete = this._currentRow;
      this.dataRows.splice(parseInt(rowIndexToDelete.toString()), 1);
      this.recalculate();
      
    

      if (this.dataRows.length === 0)
      {
          this.eventSheetDeleted.emit(this.guidId);
      }

    }

    copyRow() : void {

        var sourceRowIndex = this._currentRow;
        var sourceRow = this.dataRows[sourceRowIndex];
        var numberOfRows = this.dataRows.length;
        
        for (var r = 0; r < numberOfRows; r++) 
        {
            if (r == sourceRowIndex) continue;
            var destinationRow = this.dataRows[r];
            destinationRow.CopyFrom(sourceRow);
        }

        this.recalculate();

    }

    

}
