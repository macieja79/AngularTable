import { Component, OnInit, Inject } from '@angular/core';
import { SheetRow } from "../../Model/SheetRow";
import { SheetComponent } from "../Sheet/sheet.component";
import { SheetSummaryComponent } from "../SheetSummary/sheetSummary.component";
import { EventEmitter } from "events";
import { SheetService } from "../../Services/sheet.service";
import { Consts } from "../../Model/Consts";
import { INotificationService } from "../../Services/notification.service";

@Component({
  selector: 'my-list',
  // template: `<h1>Hello {{name}}</h1>`
  styleUrls: ['./sheetList.component.css'],
  templateUrl: './sheetList.component.html'
  
})
export class SheetListComponent implements OnInit {

    constructor(
      private sheetService : SheetService,
      @Inject('INotificationService') private notificationService: INotificationService 
    ) {}

    private _counter : number = 0;
    

    sheets : SheetComponent[] = [];
    totalSummaryTest: SheetRow = SheetRow.Create(Consts.NumberOfColumns);

    ngOnInit(): void {
      this.addNewSheet();
    }

    handleAddButtonClick() : void {
      this.addNewSheet();
    }

    handleEventLastRowDeleted(data:any) : void {
        this.sheets.splice(0, 1);
    }

    handleEventCellValueChanged(data:any) : void {
      this.updateTotalSummary();
    }

    handleEventSheetDeleted(guid : string) : void{

      console.log(guid);

      var index = -1;
      var guid : string;
      for (var i = 0; i < this.sheets.length; i++)
      {
          console.log(this.sheets[i].guidId);

          if (this.sheets[i].guidId == guid)
          {
            index = i;
            break;
          }
      }

      if (index >= 0)
      {
         this.sheets.splice(index, 1);
         this.sheetService.deleteEntry(guid);
         this.updateTotalSummary();
         this.notificationService.Message("Removed!");
      }
    }

    handleShowExample() : void {
      this.notificationService.Message("Message from button!")

    }

    addNewSheet() : void {
        var newItem = new SheetComponent(this.sheetService);
        this.sheets.push(newItem);
    }

    updateTotalSummary() : void{
           this.totalSummaryTest = this.sheetService.getTotalSummary(); 
    }

    public isToShowSummary() : boolean {
        var numberOfSheets = this.sheets.length;
        return numberOfSheets > 1;
    }

    public testMethod() : void {
      var something : number = 0;
      // this is my test
    }
}