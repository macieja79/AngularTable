import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

// components

import { SheetComponent } from './Components/Sheet/sheet.component'
import { SheetSummaryComponent } from "./Components/SheetSummary/sheetSummary.component";
import { SheetListComponent } from './Components/SheetList/sheetList.component'
import { SheetService } from "./Services/sheet.service";
import { AlertService } from "./Services/notification.service.alert";
import { BootboxService } from "./Services/notification.service.bootbox";



@NgModule({
  imports:      [ BrowserModule, FormsModule],
  declarations: [ SheetListComponent,  SheetComponent, SheetSummaryComponent ],
  providers:    [ SheetService, {provide: 'INotificationService', useClass: BootboxService}],
  bootstrap:    [ SheetListComponent ]
})
export class AppModule { }
