import { Injectable } from "@angular/core";
import { SheetRow } from "../Model/SheetRow";
import { Consts } from "../Model/Consts";

@Injectable()
export class SheetService {
    
    private summaryRowPairs : GuidSummaryRowPair[] = [];

    updateSummary(sheetId:string, data : SheetRow) : void{

        for(let index in this.summaryRowPairs)
        {
            var pair = this.summaryRowPairs[index];
            if (pair.guid === sheetId)
            {
                pair.row = data;
                return;
            }
        }

        var newPair = new GuidSummaryRowPair();
        newPair.guid = sheetId;
        newPair.row = data;
        this.summaryRowPairs.push(newPair);
    }

    deleteEntry(sheetId: string) {

        var i = 0;
        for(let index in this.summaryRowPairs)
        {
            var pair = this.summaryRowPairs[index];
            if (pair.guid === sheetId)
            {
                this.summaryRowPairs.splice(i,1);
                return;
            }

            i++;
        }

    }

    getTotalSummary() : SheetRow {

        var column : number = 0;
        var totalSummary: SheetRow = SheetRow.Create(Consts.NumberOfColumns);

        for (let index in this.summaryRowPairs)
        {
            var pair = this.summaryRowPairs[index];
            totalSummary.Sum(pair.row);
        }

        return totalSummary;
    }
    
    private _counter : number = 0;
    getName() : string {
            return "Table_" + this._counter++;
    }
}

// helper class for managing collection of summaries
class GuidSummaryRowPair
{
    guid : string;
    row: SheetRow;
}