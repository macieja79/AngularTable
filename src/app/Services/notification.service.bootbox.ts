import { INotificationService } from "./notification.service";
declare var bootbox:any; 

export class BootboxService implements INotificationService
{
        Message(text: string): void {
           bootbox.alert(text);
        }
}